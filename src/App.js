import React, { Component } from 'react'
import LoginContainer from './containers/LoginContainer'
import SignUpContainer from './containers/SignUpContainer'
import { BrowserRouter as Router } from 'react-router-dom'
import DashboardContainer from './containers/DashboardContainer'
import { PrivateRoute, NotLoginRoute } from './components/CustomRoutes'
export default class App extends Component {
  render() {
    return (
      <Router>
        <NotLoginRoute path="/signup" component={SignUpContainer} />
        <NotLoginRoute path="/" exact component={LoginContainer} />
        <PrivateRoute path="/dashboard" component={DashboardContainer} />
      </Router>
    )
  }
}
