import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(2),
  }
}))

export default function Class({ className, amount, code, _id, props }) {
  const classes = useStyles()

  return (
    <div
      onClick={() => {
        props.location.search = _id
        props.history.push(`/dashboard/classes/${_id}`)
      }}
    >
      <Paper elevation={4}  square className={classes.root}>
        <Typography variant="h4" component="h3">
          Lớp {className}
        </Typography>
        <Typography component="p">Mã: {code}</Typography>
        <Typography component="p">Sĩ số: {amount}</Typography>
      </Paper>
    </div>
  )
}
