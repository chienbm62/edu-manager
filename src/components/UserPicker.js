import React from 'react'

import Autosuggest from 'react-autosuggest'
import AutosuggestHighlightMatch from 'autosuggest-highlight/umd/match'
import AutosuggestHighlightParse from 'autosuggest-highlight/umd/parse'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem'
import { makeStyles } from '@material-ui/core/styles'
import axios from '../config/axiosConfig'
import { inject, observer } from 'mobx-react'
import { get } from 'lodash'
var users = []
function renderInputComponent(inputProps) {
  const { classes, inputRef = () => {}, ref, ...other } = inputProps

  return (
    <TextField
      fullWidth
      InputProps={{
        inputRef: node => {
          ref(node)
          inputRef(node)
        },
        classes: {
          input: classes.input
        }
      }}
      {...other}
    />
  )
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = AutosuggestHighlightMatch(suggestion.displayName, query)
  const parts = AutosuggestHighlightParse(suggestion.displayName, matches)

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        {parts.map(part => (
          <span key={part.text} style={{ fontWeight: part.highlight ? 500 : 400 }}>
            {part.text}
          </span>
        ))}
      </div>
    </MenuItem>
  )
}

function getSuggestions(value) {
  const inputValue = value
  const inputLength = inputValue.length
  let count = 0

  return inputLength === 0
    ? []
    : users.filter(user => {
        const keep =
          count < 5 &&
          get(user, 'displayName', '')
            .toLowerCase()
            .includes(inputValue)

        if (keep) {
          count += 1
        }

        return keep
      })
}

// phần này sẽ chạy mỗi khi chọn được giá trị mình cần
// viết phần thêm vào data của bảng ở đây
// students.push(suggestion)
function getSuggestionValue(suggestion) {
  return suggestion.displayName
}

const useStyles = makeStyles(theme => ({
  root: {
    height: 100,
    flexGrow: 1
  },
  container: {
    position: 'relative'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0
  },
  suggestion: {
    display: 'block'
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  },
  divider: {
    height: theme.spacing(2)
  }
}))
const UserPicker = inject('RootStore')(
  observer(props => {
    const classes = useStyles()
    const [state, setState] = React.useState({
      single: ''
    })
    const { RootStore } = props
    const [stateSuggestions, setSuggestions] = React.useState([])

    if (!users.length) {
      axios
        .get('/users')
        .then(res => {
          users = res.data
        })
        .catch(err => {
          console.log(err)
        })
    }

    const handleSuggestionsFetchRequested = ({ value }) => {
      setSuggestions(getSuggestions(value))
    }

    const handleSuggestionsClearRequested = () => {
      setSuggestions([])
    }

    const handleChange = name => (event, { newValue }) => {
      setState({
        ...state,
        [name]: newValue
      })
    }
    const handleSuggestionSelected = (event, { suggestionValue }) => {
      RootStore.addStudents(suggestionValue)
    }
    const autosuggestProps = {
      renderInputComponent,
      suggestions: stateSuggestions,
      onSuggestionsFetchRequested: handleSuggestionsFetchRequested,
      onSuggestionsClearRequested: handleSuggestionsClearRequested,
      getSuggestionValue,
      renderSuggestion,
      onSuggestionSelected: handleSuggestionSelected
    }

    return (
      <div className={classes.root}>
        <Autosuggest
          {...autosuggestProps}
          inputProps={{
            width: '100%',
            classes,
            variant: 'outlined',
            margin: 'normal',
            id: 'react-autosuggest-simple',
            label: 'Chọn sinh viên',
            value: state.single,
            onChange: handleChange('single')
          }}
          theme={{
            container: classes.container,
            suggestionsContainerOpen: classes.suggestionsContainerOpen,
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion
          }}
          renderSuggestionsContainer={options => (
            <Paper {...options.containerProps} square>
              {options.children}
            </Paper>
          )}
        />
      </div>
    )
  })
)
export default UserPicker
