import axios from 'axios'
import { getDomain } from '../shared/helper'

const instance = axios.create({
  baseURL: getDomain()
})

instance.interceptors.request.use(function(config) {
  const token = localStorage.getItem('token', '')
  config.headers.Authorization = token ? `Bearer ${token}` : ''

  return config
})

export default instance
