import React, { Component } from 'react'
import axios from '../config/axiosConfig'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Spinner from 'react-spinner-material'
import { Grid, Typography, Button, Paper } from '@material-ui/core'
import ReactTable from 'react-table'
import 'react-table/react-table.css'

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing(2)
  },
  paper: {
    margin: theme.spacing(2),
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary
  }
})

function AttendanceStudents(props) {
  const students = props.students || []
  if (!students.length) {
    return (
      <Button variant="contained" color="secondary">
        Thêm sinh viên
      </Button>
    )
  }
  const columns = [
    {
      Header: 'STT',
      id: 'index',
      Cell: row => {
        return <div>{row.index + 1}</div>
      }
    },
    {
      Header: 'Họ và tên',
      accessor: 'displayName'
    },
    {
      Header: 'Email',
      accessor: 'email'
    },
    {
      Header: 'Lớp',
      accessor: 'officalClass'
    }
  ]
  return (
    <div>
      <ReactTable
        data={props.students}
        columns={columns}
        className="-highlight"
        resolveData={data => data.map(row => row)}
        showPagination={false}
        pageSizeOptions={10}
        defaultPageSize={10}
        noDataText="Chưa có sinh viên nào"
        resizable={false}
      ></ReactTable>
      <Button variant="contained" color="secondary" style={{ margin: 5}}>
        Thêm sinh viên
      </Button>
    </div>
  )
}
class AttendanceContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedClass: null
    }
  }
  componentDidMount() {
    axios
      .get(`/classes/${this.props.match.params._id}`)
      .then(res => {
        this.setState({ selectedClass: { ...res.data } })
      })
      .catch(err => console.log(err))
  }

  render() {
    const { classes } = this.props
    if (this.props.location === '/dashboard/classes' && !this.state.selectedClass) {
      return (
        <Grid container direction="row" justify="center" alignItems="center">
          <Spinner size={40} spinnerColor={'#333'} spinnerWidth={2} visible={true} />
        </Grid>
      )
    }

    return (
      <div className={classes.root}>
        <Grid spacing={3}>
          <Typography variant="h4" component="h2">
            Thông tin
          </Typography>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <Typography component="p" variant="h5">
                Lớp: {this.state.selectedClass && this.state.selectedClass.className}
              </Typography>
              <Typography component="p" variant="h5">
                Mã: {this.state.selectedClass && this.state.selectedClass.code}
              </Typography>
              <Typography component="p" variant="h5">
                Số lượng: {this.state.selectedClass && this.state.selectedClass.amount}
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <AttendanceStudents
                students={this.state.selectedClass && this.state.selectedClass.students}
              />
            </Paper>
          </Grid>
        </Grid>
      </div>
    )
  }
}
AttendanceContainer.propTypes = {
  classes: PropTypes.object.isRequired
}
export default withStyles(styles)(AttendanceContainer)
