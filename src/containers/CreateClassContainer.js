import React, { Component } from 'react'

import { Container, Grid, TextField, Typography, Button } from '@material-ui/core'
import UserPicker from '../components/UserPicker'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import { inject, observer } from 'mobx-react'
import axios from '../config/axiosConfig'

@inject('RootStore')
@observer
class CreateClassContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.handleClassNameChange = this.handleClassNameChange.bind(this)
    this.handleCodeChange = this.handleCodeChange.bind(this)
  }
  handleClassNameChange(e) {
    this.setState({ ...this.state, className: e.target.value })
  }
  handleCodeChange(e) {
    this.setState({ ...this.state, code: e.target.value })
  }
  handleCreateClass() {
    const students = this.props.RootStore.loadStudents
    axios
      .post('/classes', {
        className: this.state.className,
        code: this.state.code,
        students: students.map(student => student._id)
      })
      .then(res => console.log(res))
      .catch(err => console.error(err))
  }
  render() {
    const { RootStore } = this.props
    const columns = [
      {
        Header: 'STT',
        id: 'index',
        Cell: row => {
          return <div>{row.index + 1}</div>
        },
        maxWidth: 50,
        style: {
          textAlign: 'center'
        },
        headerStyle: {
          backgroundColor: '#4fa4f4'
        }
      },
      {
        Header: 'Tên hiển thị',
        accessor: 'displayName',
        style: {
          textAlign: 'center'
        },
        headerStyle: {
          backgroundColor: '#4fa4f4'
        }
      },
      {
        Header: 'Lớp',
        accessor: 'officalClass',
        style: {
          textAlign: 'center'
        },
        headerStyle: {
          backgroundColor: '#4fa4f4'
        }
      },
      {
        Header: 'Email',
        accessor: 'email',
        headerStyle: {
          backgroundColor: '#4fa4f4'
        },
        style: {
          textAlign: 'center'
        }
      }
    ]
    return (
      <div>
        <Container maxWidth="md">
          <Grid>
            <Typography variant="h4" component="h2">
              Thông tin
            </Typography>
          </Grid>
          <Grid>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="classname"
              label="Tên lớp"
              name="classname"
              autoFocus
              onChange={this.handleClassNameChange}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="code"
              label="Mã lớp"
              name="code"
              autoFocus
              onChange={this.handleCodeChange}
            />
            <UserPicker></UserPicker>
            <ReactTable
              className="-highlight"
              data={RootStore.loadStudents} // The data prop should be immutable and only change when you want to update the table
              resolveData={data => data.map(row => row)}
              columns={columns}
              showPagination={false}
              pageSizeOptions={10}
              defaultPageSize={10}
              noDataText="Chưa có sinh viên nào"
              resizable={false}
            />
            <Button
              style={{ marginTop: '20px', marginLeft: '48%' }}
              variant="contained"
              color="primary"
              onClick={() => {
                this.handleCreateClass()
              }}
            >
              Tạo
            </Button>
          </Grid>
        </Container>
      </div>
    )
  }
}

export default CreateClassContainer
