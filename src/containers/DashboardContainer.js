import React from 'react'
import { fade, makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import InputBase from '@material-ui/core/InputBase'
import Badge from '@material-ui/core/Badge'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import MenuIcon from '@material-ui/icons/Menu'
import SearchIcon from '@material-ui/icons/Search'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MailIcon from '@material-ui/icons/Mail'
import NotificationsIcon from '@material-ui/icons/Notifications'
import MoreIcon from '@material-ui/icons/MoreVert'
import { inject, observer } from 'mobx-react'
import { Route, Switch } from 'react-router-dom'
import ListClassContainer from './ListClassContainer'
import AttendanceContainer from './AttendanceContainer'
import CreateClassContainer from './CreateClassContainer'
import UserProfileContainer from './UserProfileContainer'

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto'
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200
    }
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  }
}))
const PrimarySearchAppBar = inject('RootStore')(
  observer(props => {
    const classes = useStyles()
    const [anchorEl, setAnchorEl] = React.useState(null)
    const [leftMenu, setleftMenu] = React.useState(null)
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null)
    const isMenuOpen = Boolean(anchorEl)
    const isLeftMenuOpen = Boolean(leftMenu)
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)

    function handleLogout() {
      localStorage.removeItem('token')
      props.history.push('/')
    }
    function handleProfileMenuOpen(event) {
      setAnchorEl(event.currentTarget)
    }
    function handleLeftMenuOpen(event) {
      setleftMenu(event.currentTarget)
    }

    function handleMobileMenuClose() {
      setMobileMoreAnchorEl(null)
    }

    function handleMenuClose() {
      setAnchorEl(null)
      handleMobileMenuClose()
    }
    function handleRedirectToListClass() {
      setleftMenu(null)
      props.history.push('/dashboard/classes')
    }
    function handleLeftMenuClose() {
      setleftMenu(null)
      handleMobileMenuClose()
    }

    function handleRedirectToUserProfile() {
      setAnchorEl(null)
      props.history.push('/dashboard/profile')
    }
    function handleMobileMenuOpen(event) {
      setMobileMoreAnchorEl(event.currentTarget)
    }

    const menuId = 'primary-search-account-menu'
    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={menuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={handleMenuClose}
        PaperProps={{
          style: {
            maxHeight: 200 * 3,
            width: 200
          }
        }}
      >
        <MenuItem onClick={handleRedirectToUserProfile}>My Account</MenuItem>
        <MenuItem onClick={handleLogout}>Đăng xuất</MenuItem>
      </Menu>
    )
    const renderLeftMenu = (
      <Menu
        anchorEl={leftMenu}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        id={menuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'left' }}
        open={isLeftMenuOpen}
        PaperProps={{
          style: {
            maxHeight: 200 * 3,
            width: 200
          }
        }}
        onClose={handleLeftMenuClose}
      >
        <MenuItem onClick={handleRedirectToListClass}>Danh sách lớp</MenuItem>
        <MenuItem onClick={handleMenuClose}>Tài liệu</MenuItem>
      </Menu>
    )

    const mobileMenuId = 'primary-search-account-menu-mobile'
    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={mobileMenuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}
      >
        <MenuItem>
          <IconButton aria-label="show 4 new mails" color="inherit">
            <Badge badgeContent={4} color="secondary">
              <MailIcon />
            </Badge>
          </IconButton>
          <p>Messages</p>
        </MenuItem>
        <MenuItem>
          <IconButton aria-label="show 11 new notifications" color="inherit">
            <Badge badgeContent={11} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
        <MenuItem onClick={handleProfileMenuOpen}>
          <IconButton
            aria-label="account of current user"
            aria-controls="primary-search-account-menu"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
    )

    return (
      <div className={classes.grow}>
        <AppBar position="static" style={{ width: '100%', boxShadow: 'none' }}>
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="open drawer"
              onClick={handleLeftMenuOpen}
            >
              <MenuIcon />
            </IconButton>
            <Typography className={classes.title} variant="h6" noWrap>
              Edu-manager
            </Typography>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
                inputProps={{ 'aria-label': 'search' }}
              />
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <IconButton aria-label="show 4 new mails" color="inherit">
                <Badge badgeContent={4} color="secondary">
                  <MailIcon />
                </Badge>
              </IconButton>
              <IconButton aria-label="show 17 new notifications" color="inherit">
                <Badge badgeContent={17} color="secondary">
                  <NotificationsIcon />
                </Badge>
              </IconButton>
              <IconButton
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
              >
                <AccountCircle />
                <Typography className={classes.title} variant="h6" noWrap>
                  {localStorage.getItem('displayName')}
                </Typography>
              </IconButton>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                color="inherit"
              >
                <MoreIcon />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
        {renderLeftMenu}
        <Switch>
          <Route exact path="/dashboard/classes" component={ListClassContainer} />
          <Route exact path="/dashboard/classes/create" component={CreateClassContainer} />
          <Route exact path="/dashboard/classes/:_id" component={AttendanceContainer} />
          <Route exact path="/dashboard/profile" component={UserProfileContainer} />
        </Switch>
      </div>
    )
  })
)
export default PrimarySearchAppBar
