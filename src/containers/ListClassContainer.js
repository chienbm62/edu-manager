import React, { Component } from 'react'
import Class from '../components/Class'
import axios from '../config/axiosConfig'
import { Grid, Button } from '@material-ui/core'
import { map } from 'lodash'

export default class ListClassContainer extends Component {
  componentDidMount() {
    axios
      .get('/classes')
      .then(res => {
        this.setState({ classes: res.data, dataLoaded: true })
      })
      .catch(function(error) {
        console.log(error)
      })
  }
  constructor(props) {
    super(props)
    this.state = { classes: [], dataLoaded: false }
  }
  render() {
    return (
      <Grid container>
        <Grid item xs={12}>
          {map(this.state.classes, (e, index) => (
            <Class
              key={index}
              props={this.props}
              className={e.className}
              amount={e.amount}
              code={e.code}
              _id={e._id}
            />
          ))}
        </Grid>
        <Grid item container justify="center">
          <Button
            style={buttonStyle}
            variant="contained"
            color="secondary"
            onClick={() => this.props.history.push('/dashboard/classes/create')}
          >
            Tạo lớp mới
          </Button>
        </Grid>
      </Grid>
    )
  }
}

const buttonStyle = {
  marginTop: 10
}
