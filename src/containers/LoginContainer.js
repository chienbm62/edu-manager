import React, { Component } from 'react'
import {
  Container,
  Grid,
  Typography,
  TextField,
  Button,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { withSnackbar } from 'notistack'

@inject('RootStore')
@observer
class LoginContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {
        email: '',
        password: ''
      }
    }
    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
  }
  handleEmailChange(e) {
    this.setState({ user: { ...this.state.user, email: e.target.value } })
  }

  handlePasswordChange(e) {
    this.setState({ user: { ...this.state.user, password: e.target.value } })
  }
  render() {
    const { RootStore } = this.props
    return (
      <Container component="main" maxWidth="xs">
        <Grid>
          <Grid>
            <Typography style={typoStyle} color="primary" component="h1" variant="h5">
              Đăng nhập
            </Typography>
          </Grid>
          <form >
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              autoFocus
              onChange={this.handleEmailChange}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={this.handlePasswordChange}
            />
            <Grid container>
              <Grid container justify="center" spacing={1}>
                <Grid item>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    onClick={(e) => {
                      RootStore.login(this.state.user.email, this.state.user.password, () => {
                        this.props.history.push('/dashboard')
                        this.props.enqueueSnackbar('Đăng nhập thành công!', { variant: 'success' })
                      })
                      e.preventDefault()
                    }}
                  >
                    Đăng nhập
                  </Button>
                </Grid>
                <Grid item>
                  <Link style={{ textDecoration: 'none' }} to="/signup">
                    <Button variant="contained" color="secondary">
                      Đăng kí
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
          </form>
        </Grid>
      </Container>
    )
  }
}

export default withSnackbar(LoginContainer)
const typoStyle = {
  textAlign: 'center'
}
