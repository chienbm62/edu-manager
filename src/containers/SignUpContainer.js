import React, { Component, useState } from 'react'
import {
  Container,
  Grid,
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  OutlinedInput,
  FormControlLabel,
  Radio,
  RadioGroup,
  FormLabel
} from '@material-ui/core'
import axios from '../config/axiosConfig'
import schoolsData from '../stores/ExampleData'
import { useSnackbar } from 'notistack'

const FacultyAndClassInput = props => {
  const { type, handleChangeClass, handleChangeFaculty } = props
  if (type === 'lecturer') return <div></div>
  return (
    <div>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="faculty"
        label="Khoa"
        type="text"
        id="faculty"
        onChange={e => handleChangeFaculty(e)}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="class"
        label="Lớp"
        type="text"
        id="class"
        onChange={e => handleChangeClass(e)}
      />
    </div>
  )
}

function FormByType(props) {
  const [schools, setSchools] = useState(schoolsData)
  const [submitForm, setSubmitForm] = useState({})
  const { enqueueSnackbar } = useSnackbar()
  function handleChangeFaculty(e) {
    setSubmitForm({ ...submitForm, faculty: e.target.value })
  }
  function handleChangeClass(e) {
    setSubmitForm({ ...submitForm, officalClass: e.target.value })
  }
  const signup = () => {
    const user = { ...submitForm, type: props.type }
    axios
      .post('/users', user)
      .then(res => {
        props.history.push('/')
        enqueueSnackbar('Đăng kí thành công!', { variant: 'success' })
      })
      .catch(function(error) {
        console.log(error)
        enqueueSnackbar(`${error.message}`, { variant: 'error' })
      })
  }

  return (
    <Grid>
      <FormControl fullWidth size="large" variant="outlined">
        <InputLabel htmlFor="outlined-age-native-simple">Trường</InputLabel>
        <Select
          native
          onChange={e => {
            setSubmitForm({ ...submitForm, school: e.target.value })
          }}
          input={<OutlinedInput name="age" id="outlined-age-native-simple" />}
        >
          <option value="" />
          {schools.map(school => (
            <option value={school.name}>{school.name}</option>
          ))}
        </Select>
      </FormControl>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="email"
        label="Email"
        name="email"
        autoFocus
        onChange={e => {
          setSubmitForm({ ...submitForm, email: e.target.value })
        }}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        onChange={e => {
          setSubmitForm({ ...submitForm, password: e.target.value })
        }}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="lastname"
        label="Họ"
        type="text"
        id="lastname"
        onChange={e => {
          setSubmitForm({ ...submitForm, lastName: e.target.value })
        }}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="firstname"
        label="Tên"
        type="text"
        id="firstname"
        onChange={e => {
          setSubmitForm({ ...submitForm, firstName: e.target.value })
        }}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="cellphone"
        label="SĐT"
        type="text"
        id="cellphone"
        onChange={e => {
          setSubmitForm({ ...submitForm, cellphone: e.target.value })
        }}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="date"
        label="Ngày sinh"
        type="date"
        InputLabelProps={{
          shrink: true
        }}
      />
      <FormLabel component="legend">Giới tính</FormLabel>
      <RadioGroup
        aria-label="Gender"
        name="gender"
        onChange={e => {
          setSubmitForm({ ...submitForm, gender: e.target.value })
        }}
        row
      >
        <FormControlLabel value="male" control={<Radio />} label="Nam" />
        <FormControlLabel value="female" control={<Radio />} label="Nữ" />
        <FormControlLabel value="other" control={<Radio />} label="Khác" />
      </RadioGroup>
      <FacultyAndClassInput
        type={props.type}
        handleChangeFaculty={handleChangeFaculty}
        handleChangeClass={handleChangeClass}
      ></FacultyAndClassInput>
      <Button
        variant="contained"
        color="primary"
        style={{ marginLeft: '44%' }}
        onClick={() => {
          signup()
        }}
      >
        Đăng kí
      </Button>
    </Grid>
  )
}

class SignUpContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      type: 'student'
    }
  }

  handleOption(type) {
    this.setState({
      type: type
    })
  }

  render() {
    return (
      <Container component="main" maxWidth="md">
        <Grid>
          <Grid item>
            <Button
              size="large"
              variant="outlined"
              color="primary"
              aria-label="Large Outlined secondary button "
              onClick={() => {
                this.handleOption('student')
              }}
            >
              Sinh viên
            </Button>

            <Button
              size="large"
              variant="outlined"
              color="primary"
              aria-label="Large Outlined secondary button "
              onClick={() => {
                this.handleOption('lecturer')
              }}
            >
              Giảng viên
            </Button>
          </Grid>
          <Grid>
            <FormByType type={this.state.type} history={this.props.history} />
          </Grid>
        </Grid>
      </Container>
    )
  }
}
export default SignUpContainer
