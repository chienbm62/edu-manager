import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Avatar from '@material-ui/core/Avatar'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import axios from '../config/axiosConfig'
import '../style.css'
import moment from 'moment'
import { find } from 'lodash'

const useStyles = makeStyles(theme => ({
  avatar: {
    marginTop: 50,
    marginLeft: 100,
    width: 200,
    height: 200
  },
  paper: {
    marginTop: 50,
    marginRigth: 100,
    width: 600,
    padding: 5
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 580
  }
}))
export default function UserProfileContainer() {
  const genderMap = [
    { gender: 'male', display: 'Nam' },
    { gender: 'female', display: 'Nữ' },
    { gender: 'other', display: 'Khác' }
  ]
  const classes = useStyles()
  const [user, setUser] = useState({})
  useEffect(() => {
    const fetchData = async () => {
      const res = await axios.get(`/users/${localStorage.getItem('_id')}`)
      const gender = find(genderMap, { gender: res.data.gender })
      setUser({
        ...res.data,
        birthyear: moment(res.data.birthyear).format('DD-MM-YYYY'),
        gender: gender.display
      })
    }
    fetchData()
  }, {})
  const link =
    'https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/51054729_2263977140589993_8018475827328974848_n.jpg?_nc_cat=103&_nc_oc=AQn-rb7mk_sxvOqhhNqwgDBQvHdqt_zeuCtDKtZxboVY1ZPA-ITTUKxDqhTtvF28Swo&_nc_ht=scontent.fhan2-1.fna&oh=b8af919263d4583588fc2c8ee3230b81&oe=5E1010AA'
  return (
    <Grid container spacing={3}>
      <Grid item xs={5}>
        <Avatar alt="Remy Sharp" src={link} className={classes.avatar} />
      </Grid>
      <Grid item xs={7}>
        <Paper className={classes.paper}>
          <div className="panel panel-label">
            <label>Tên đầy đủ </label>
            <span className="disabled-span">{user.displayName}</span>
          </div>
          <div className="panel panel-label">
            <label>Trường</label>
            <span className="disabled-span">{user.school}</span>
          </div>
          <div className="panel panel-label">
            <label>Email</label>
            <span className="disabled-span">{user.email}</span>
          </div>
          <div className="panel panel-label">
            <label>Ngày sinh</label>
            <span className="disabled-span">{user.birthyear}</span>
          </div>
          <div className="panel panel-label">
            <label>Giới tính</label>
            <span className="disabled-span">{user.gender}</span>
          </div>
          <div className="panel panel-label">
            <label>Lớp</label>
            <span className="disabled-span">{user.officalClass}</span>
          </div>
          <div className="panel panel-label">
            <label>SĐT</label>
            <span className="disabled-span">{user.cellphone}</span>
          </div>
        </Paper>
      </Grid>
    </Grid>
  )
}
