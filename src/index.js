import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter } from 'react-router-dom'
import { MuiThemeProvider } from '@material-ui/core'
import theme from './themes/default'
import { Provider } from 'mobx-react'
import RootStore from './stores/RootStore'
import WebFont from 'webfontloader'
import { SnackbarProvider } from 'notistack'

WebFont.load({
  google: {
    families: ['Montserrat', 'Arial']
  }
})
ReactDOM.render(
  <BrowserRouter>
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider
        maxSnack={3}
        anchorOrigin={{
          autoHideDuration: '2000',
          disableWindowBlurListener: true,
          vertical: 'bottom',
          horizontal: 'right'
        }}
      >
        <Provider RootStore={RootStore}>
          <App />
        </Provider>
      </SnackbarProvider>
    </MuiThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
)
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
