exports.getDomain = function() {
  if (process.env.NODE_ENV === 'development') return 'http://localhost:4000'
  return 'https://edu-manager-server.herokuapp.com'
}
