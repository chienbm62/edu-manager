const schools = [
  {
    name: 'Trường Đại học Bách khoa'
  },
  {
    name: 'Trường Đại học Công đoàn'
  },
  {
    name: 'Trường Đại học Công nghệ'
  },
  {
    name: 'Trường Đại học Công nghệ Giao thông Vận tải'
  },
  {
    name: 'Trường Đại học Công nghệ Dệt may'
  },
  {
    name: 'Trường Đại học Công nghiệp'
  },
  {
    name: 'Đại học Công nghiệp Việt Hung'
  },
  {
    name: 'Trường Đại học Dược'
  },
  {
    name: 'Trường Đại học Điện lực'
  },
  {
    name: 'Trường Đại học Giao thông Vận tải'
  },
  {
    name: 'Trường Đại học Giáo dục'
  },
  {
    name: 'Trường Đại học Hà Nội'
  },
  {
    name: 'Trường Đại học Khoa học Tự nhiên'
  },
  {
    name: 'Trường Đại học Khoa học và Công nghệ'
  },
  {
    name: 'Trường Đại học Khoa học Xã hội và Nhân văn'
  },
  {
    name: 'Trường Đại học Kiểm sát'
  },
  {
    name: 'Trường Đại học Kiến trúc'
  },
  {
    name: 'Trường Đại học Kinh tế'
  },
  {
    name: 'Trường Đại học Kinh tế – Kỹ thuật Công nghiệp'
  },
  {
    name: 'Trường Đại học Kinh tế Quốc dân'
  },
  {
    name: 'Trường Đại học Kỹ thuật Lê Quý Đôn'
  },
  {
    name: 'Trường Đại học Lao động – Xã hội'
  },
  {
    name: 'Trường Đại học Lâm nghiệp Việt Nam'
  },
  {
    name: 'Trường Đại học Luật'
  },
  {
    name: 'Trường Đại học Mỏ – Địa chất'
  },
  {
    name: 'Đại học Mỹ thuật Công nghiệp'
  },
  {
    name: 'Trường Đại học Mỹ thuật Việt Nam'
  },
  {
    name: 'Trường Đại học Ngoại ngữ'
  },
  {
    name: 'Trường Đại học Ngoại thương'
  },
  {
    name: 'Trường Đại học Nội vụ Hà Nội'
  },
  {
    name: 'Trường Đại học Răng Hàm Mặt'
  },
  {
    name: 'Trường Đại học Sân khấu – Điện ảnh'
  },
  {
    name: 'Trường Đại học Sư phạm'
  },
  {
    name: 'Trường Đại học Sư phạm Nghệ thuật Trung ương'
  },
  {
    name: 'Trường Đại học Sư phạm Thể dục Thể thao'
  },
  {
    name: 'Trường Đại học Thủ đô'
  },
  {
    name: 'Trường Đại học Thủy lợi'
  },
  {
    name: 'Trường Đại học Thương mại'
  },
  {
    name: 'Trường Đại học Văn hóa'
  },
  {
    name: 'Trường Đại học Xây dựng'
  },
  {
    name: 'Trường Đại học Y'
  },
  {
    name: 'Trường Đại học Y tế Công cộng'
  },
  {
    name: 'Đại học Tài nguyên và Môi trường'
  },
  {
    name: 'Trường Đại học Tài chính Ngân hàng'
  },
  {
    name: 'Trường Đại học Thành Đô'
  },
  {
    name: 'Trường Đại học Thành Tây'
  },
  {
    name: 'Trường Đại học Thăng Long'
  },
  {
    name: 'Đại học Phương Đông'
  },
  {
    name: 'Trường Đại học Quốc tế Bắc Hà'
  },
  {
    name: 'Trường Đại học Đông Đô'
  },
  {
    name: 'Trường Đại học FPT'
  },
  {
    name: 'Trường Đại học Công nghệ và Quản lý Hữu nghị'
  },
  {
    name: 'Đại học RMIT Việt Nam'
  },
  {
    name: 'Trường Đại học Nguyễn Trãi'
  },
  {
    name: 'Trường Đại học Hòa Bình'
  },
  {
    name: 'Trường Đại học Đại Nam'
  },
  {
    name: 'Trường Đại học Kinh doanh và Công nghệ'
  },
  {
    name: 'Học viện Âm nhạc Quốc gia Việt Nam'
  },
  {
    name: 'Học viện Múa Việt Nam'
  },
  {
    name: 'Học viện Báo chí và Tuyên truyền'
  },
  {
    name: 'Học viện Biên phòng'
  },
  {
    name: 'Học viện Công nghệ Thông tin Bách Khoa'
  },
  {
    name: 'Học viện Công nghệ Bưu chính Viễn thông'
  },
  {
    name: 'Học viện Chính trị'
  },
  {
    name: 'Học viện Chính trị – Hành chính Quốc gia Hồ Chí Minh'
  },
  {
    name: 'Học viện Hành chính Quốc gia'
  },
  {
    name: 'Học viện Kỹ thuật Mật mã'
  },
  {
    name: 'Học viện Ngân hàng'
  },
  {
    name: 'Học viện Ngoại giao'
  },
  {
    name: 'Học viện Nông nghiệp Việt Nam'
  },
  {
    name: 'Học viện Phụ nữ Việt Nam'
  },
  {
    name: 'Học viện Tòa án'
  },
  {
    name: 'Học viện Tư pháp Việt Nam'
  },
  {
    name: 'Học viện Tài chính'
  },
  {
    name: 'Học viện Quân y'
  },
  {
    name: 'Học viện Y dược học cổ truyền Việt Nam'
  },
  {
    name: 'Học viện Chính sách và Phát triển'
  },
  {
    name: 'Học viện Thanh thiếu niên Việt Nam'
  },
  {
    name: 'Viện Đại học Mở'
  },
  {
    name: 'Học viện An ninh Nhân dân'
  },
  {
    name: 'Học viện Cảnh sát Nhân dân'
  },
  {
    name: 'Học viện Phòng không – Không quân'
  },
  {
    name: 'Trường Sĩ quan Đặc công'
  },
  {
    name: 'Trường Sĩ quan Pháo binh'
  },
  {
    name: 'Trường Sĩ quan Phòng hóa'
  },
  {
    name: 'Sĩ quan Lục quân 1'
  },
  {
    name: 'Học viện Quản lý Giáo dục'
  }
]
export default schools
