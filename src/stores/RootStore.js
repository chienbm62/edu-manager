import { observable, action, computed } from 'mobx'
import axios from '../config/axiosConfig'
import { find, map } from 'lodash'

class RootStore {
  @observable name = ''
  @observable students = []
  @action login(email, password, action) {
    axios
      .post('/users/login', { email: email, password: password })
      .then(res => {
        this.displayName = res.data.displayName
        localStorage.setItem('token', res.data.token)
        localStorage.setItem('displayName', res.data.displayName)
        localStorage.setItem('_id', res.data._id)
      })
      .then(() => {
        action()
      })
      .catch(function(error) {
        console.log(error)
      })
  }
  @action addStudents(student) {
    if (find(this.students, { displayName: student })) {
      console.log('Co roi')
      return
    }
    axios
      .get('/users', {
        params: {
          displayName: student
        }
      })
      .then(res => {
        this.students = [...this.students, res.data[0]]
      })
  }
  @computed get loadStudents() {
    return map(this.students, student => ({
      ...student
    }))
  }
}
const store = new RootStore()
export default store
