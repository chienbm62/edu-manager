import { createMuiTheme } from '@material-ui/core'
import { blue, pink } from '@material-ui/core/colors'
const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink
  },
  spacing: 10
})
export default theme
